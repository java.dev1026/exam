package study.jpa.exam.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String question;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "exam_id")
    private Exam exam;

    @OneToMany(mappedBy = "question")
    private List<QuestionOption> questionOptions = new ArrayList<>();

    public Question() {
    }

    public Question(String question, Exam exam){
        this.question = question;
        this.exam = exam;
    }

    public Question(Long id, String question, Exam exam){
        this.id = id;
        this.question = question;
        this.exam = exam;
    }

}
