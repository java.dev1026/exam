package study.jpa.exam.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String examName;

    private LocalDate examDate;

    @OneToMany(mappedBy = "exam")
    private List<Question> questions = new ArrayList<>();

    public Exam(String examName, LocalDate examDate) {
        this.examName = examName;
        this.examDate = examDate;
    }

    public Exam(Long id, String examName, LocalDate examDate) {
        this.id = id;
        this.examName = examName;
        this.examDate = examDate;
    }

    public Exam() {
    }

    @Override
    public String toString() {
        return "Exam{" +
                "id=" + id +
                ", examName='" + examName + '\'' +
                ", examDate=" + examDate +
                '}';
    }

}
