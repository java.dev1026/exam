package study.jpa.exam.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "response")
public class Response {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id")
    private Question question;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_option_id")
    private QuestionOption questionOption;

    public Response () {}
    public Response (String username, Question question, QuestionOption questionOption) {
        this.username = username;
        this.question = question;
        this.questionOption = questionOption;
    }
}
