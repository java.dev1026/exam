package study.jpa.exam.repository;

import com.querydsl.core.Query;
import com.querydsl.core.QueryFactory;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jakarta.persistence.EntityManager;
import study.jpa.exam.dto.ResultDto;
import study.jpa.exam.entity.QResponse;
import study.jpa.exam.entity.Question;
import study.jpa.exam.entity.Response;

import java.util.List;



public class ResponseCustomRepositoryImpl implements ResponseCustomRepository{

    private final EntityManager em;
    private final JPAQueryFactory query;

    public ResponseCustomRepositoryImpl(EntityManager em){
        this.em = em;
        this.query = new JPAQueryFactory(em);
    }
    @Override
    public List<Response> getResponses() {
        QResponse response = QResponse.response;
        return query.selectFrom(response)
                .fetch();
    }

    @Override
    public void getResult(Long questionId, String username) {
        QResponse response = QResponse.response;
        QResponse response1 = new QResponse("response1");

        List<Tuple> result = query
                .select(
                        response.question.question,
                        response.questionOption.option,
                        response.count(),
                        JPAExpressions.select(response1.count())
                                .from(response1)
                                .where(
                                        response1.question.eq(response.question),
                                        response1.username.eq(username),
                                        response1.questionOption.eq(response.questionOption)
                                )
                )
                .from(response)
                .where(response.question.id.eq(questionId))
                .groupBy(response.questionOption)
                .fetch();
        for (Tuple tuple : result) {
            System.out.println(tuple);
        }
    }

    @Override
    public List<ResultDto> getResultDto(Long questionId, String username) {
        QResponse response = QResponse.response;
        QResponse response1 = new QResponse("response1");

        List<ResultDto> result = query
                .select(Projections.constructor(ResultDto.class,
                        response.question.question,
                        response.questionOption.option,
                        response.count().intValue(),
                        JPAExpressions.select(response1.count().intValue())
                                .from(response1)
                                .where(
                                        response1.question.eq(response.question),
                                        response1.username.eq(username),
                                        response1.questionOption.eq(response.questionOption)
                                )
                        )
                )
                .from(response)
                .where(response.question.id.eq(questionId))
                .groupBy(response.questionOption)
                .fetch();

        return result;
    }
}
