package study.jpa.exam.repository;

import com.querydsl.core.Tuple;
import study.jpa.exam.dto.ResultDto;
import study.jpa.exam.entity.Response;

import java.util.List;

public interface ResponseCustomRepository {

    public List<Response> getResponses();
    void getResult(Long questionId, String username);
    List<ResultDto> getResultDto(Long questionId, String username);
}
