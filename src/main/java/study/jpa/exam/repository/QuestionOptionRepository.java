package study.jpa.exam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import study.jpa.exam.entity.Question;
import study.jpa.exam.entity.QuestionOption;

import java.util.List;

public interface QuestionOptionRepository extends JpaRepository<QuestionOption, Long> {
    List<QuestionOption> findAllByQuestion(Question question);
}
