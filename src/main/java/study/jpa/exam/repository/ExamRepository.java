package study.jpa.exam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import study.jpa.exam.entity.Exam;

public interface ExamRepository extends JpaRepository<Exam, Long> {
}
