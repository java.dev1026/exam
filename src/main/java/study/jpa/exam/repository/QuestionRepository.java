package study.jpa.exam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import study.jpa.exam.entity.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}
