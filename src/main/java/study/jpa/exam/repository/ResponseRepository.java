package study.jpa.exam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import study.jpa.exam.entity.Response;

public interface ResponseRepository extends JpaRepository<Response, Long>, ResponseCustomRepository {
}
