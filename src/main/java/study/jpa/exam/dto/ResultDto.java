package study.jpa.exam.dto;

import lombok.Data;

@Data
public class ResultDto {
    private String question;
    private String option;
    private int sum;
    private int count;

    public ResultDto () {}
    public ResultDto (String question, String option, int sum, int count){
        this.question = question;
        this.option = option;
        this.sum = sum;
        this.count = count;
    }

}
