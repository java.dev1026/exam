package study.jpa.exam.service;

import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;
import study.jpa.exam.entity.Exam;
import study.jpa.exam.repository.ExamRepository;
import study.jpa.exam.repository.QuestionOptionRepository;
import study.jpa.exam.repository.QuestionRepository;
import study.jpa.exam.repository.ResponseRepository;

@Service
public class ExamService {

    private final ExamRepository examRepository;
    private final QuestionRepository questionRepository;
    private final QuestionOptionRepository questionOptionRepository;
    private final ResponseRepository responseRepository;

    public ExamService(ExamRepository examRepository,
                       QuestionRepository questionRepository,
                       QuestionOptionRepository questionOptionRepository,
                       ResponseRepository responseRepository
                       ) {
        this.examRepository = examRepository;
        this.questionRepository = questionRepository;
        this.questionOptionRepository = questionOptionRepository;
        this.responseRepository = responseRepository;
    }

    @Transactional
    public Exam saveExam(Exam exam){
        examRepository.save(exam);
        exam.getQuestions().forEach(question -> {
            questionRepository.save(question);
            question.getQuestionOptions().forEach(questionOptionRepository::save);
        });
        return exam;
    }

}
