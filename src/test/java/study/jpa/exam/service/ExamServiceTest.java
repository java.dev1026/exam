package study.jpa.exam.service;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import study.jpa.exam.entity.Exam;
import study.jpa.exam.entity.Question;
import study.jpa.exam.entity.QuestionOption;
import study.jpa.exam.entity.Response;
import study.jpa.exam.repository.QuestionOptionRepository;
import study.jpa.exam.repository.QuestionRepository;
import study.jpa.exam.repository.ResponseRepository;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest
@Slf4j
class ExamServiceTest {

    @Autowired
    private ExamService examService;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionOptionRepository questionOptionRepository;

    @Autowired
    private ResponseRepository responseRepository;

    @Test
    void insertExam() {

        String examName = "수학시험";
        Exam exam = new Exam(examName, LocalDate.of(2023,3,23));
        Question question1 = new Question("1+1=", exam);
        Question question2 = new Question("2+2=", exam);
        exam.setQuestions(List.of(
                question1, question2
        ));

        question1.setQuestionOptions(List.of(
                new QuestionOption("11", question1),
                new QuestionOption("2", question1),
                new QuestionOption("0", question1)));

        question2.setQuestionOptions(List.of(
                new QuestionOption("22", question2),
                new QuestionOption("4", question2),
                new QuestionOption("0", question2)));

        Exam savedExam = examService.saveExam(exam);
        Assertions.assertThat(savedExam.getExamName()).isEqualTo(examName);
        log.info(String.valueOf(savedExam));

    }

    @Transactional
    @Commit
    @Test
    void insertResponse() {
        List<Question> questions = questionRepository.findAll();

        questions.forEach(question -> {
            List<QuestionOption> options = question.getQuestionOptions();
            Response response = new Response("길동이", question, options.get(1));
            responseRepository.save(response);
        });


    }


}