package study.jpa.exam.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import study.jpa.exam.dto.ResultDto;
import study.jpa.exam.entity.Response;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ResponseRepositoryTest {

    @Autowired
    private ResponseRepository responseRepository;

    @Test
    void getResponse(){
        List<Response> responses = responseRepository.getResponses();
        for (Response response : responses) {
            System.out.println(response);
        }
    }

    @Test
    void getResult(){
        responseRepository.getResult(1L, "둘리");
    }

    @Test
    void getResultDto(){
        List<ResultDto> results = responseRepository.getResultDto(1L, "둘리");
        for (ResultDto result : results) {
            System.out.println(result);
        }
    }
}