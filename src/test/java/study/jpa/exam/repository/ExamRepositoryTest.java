package study.jpa.exam.repository;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import study.jpa.exam.entity.Exam;

import java.time.LocalDate;

@SpringBootTest
@Slf4j
class ExamRepositoryTest {

    @Autowired
    private ExamRepository examRepository;

    void print(Exam exam) {
        log.info("****************************");
        log.info("exam = " + exam);
        log.info("****************************");
    }


    @Transactional
    @Commit
    @Test
    @DisplayName("시험을 등록한다.")
    void insertExam() {
        Exam exam = new Exam("국어시험", LocalDate.of(2024, 3, 24));
        examRepository.save(exam);
        Exam exam2 = new Exam("수학시험", LocalDate.of(2024, 3, 25));
        examRepository.save(exam2);
    }

    @Transactional
    @Commit
    @Test
    @DisplayName("수정할 시험을 조회해서 시험 정보를 수정한다.")
    void updateExam1() {
        Exam exam = examRepository.findById(1L).orElseThrow();
        exam.setExamName("국어시험-1");


    }

    @Transactional
    @Commit
    @Test
    @DisplayName("시험 아이디로 시험 정보를 수정한다.")
    void updateExam2() {
        Exam exam = new Exam("국어시험-2", LocalDate.of(2024, 3, 26));
        exam.setId(1L);
        examRepository.save(exam);

        print(exam);
    }

    @Test
    @DisplayName("@Transactional없이 수정할 시험을 조회해서 시험 정보를 수정한다.")
    void updateExam3() {
        Exam exam = examRepository.findById(1L).orElseThrow();
        exam.setExamName("국어시험-11");

        print(exam);
    }

    @Test
    @DisplayName("@Transactional없이 시험 아이디로 시험 정보를 수정한다.")
    void updateExam4() {
        Exam exam = new Exam("국어시험-22", LocalDate.of(2024, 3, 26));
        exam.setId(1L);
        examRepository.save(exam);

        print(exam);
    }

    @Test
    @DisplayName("@Transactional없이 수정할 시험을 조회해서 시험 정보를 수정한다.")
    void updateExam5() {
        Exam exam = examRepository.findById(1L).orElseThrow();
        exam.setExamName("국어시험-111");
        examRepository.save(exam);

        print(exam);
    }

}